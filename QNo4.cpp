#include <iostream>

using namespace std;

int main()
{
    long long n, k;

    cin >> n >> k;

    int num, count = 0;

    do{
        n--;
        cin >> num;
        if (num > 99 && (num % 10) % k == 0){
          
            count++;
        }
        else if(num < 100 && (num % k) == 0){
            
            count++;
        }
    }while(n);

    cout << count << endl;

    return 0;
}
